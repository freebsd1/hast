#!/bin/sh

remotehost=$1
remoteip=$2
carpip=$3
remotepool_name=$4
vol_path=$5
vol_size=$6
sharevol_name=$7

interface=$(ifconfig | grep flags | cut -d : -f 1 | awk '{print $1; exit}')
localip=$(ifconfig $interface | grep inet | awk '{print $2; exit}')

vol_name=$(echo $vol_path | cut -d / -f5)
remotevol_name=$vol_name"_replica"
remotevol_path="/dev/zvol/"$remotepool_name"/"$remotevol_name

file="/etc/hast.conf"

if [ -f $file ] ; then
  rm $file
  echo "listen tcp://0.0.0.0 " >> /etc/hast.conf
  echo "resource $sharevol_name {" >> /etc/hast.conf
  echo "on $(hostname) {" >> /etc/hast.conf
  echo "local $vol_path" >> /etc/hast.conf
  echo "remote $remoteip }" >> /etc/hast.conf
  echo "on $remotehost {" >> /etc/hast.conf
  echo "local $remotevol_path" >> /etc/hast.conf
  echo "remote $localip }" >> /etc/hast.conf
  echo "}" >> /etc/hast.conf
  echo "
fi

proc_pid=$(pgrep -lf hastd | grep sbin | awk '{print $1}')

if [ $proc_pid -ne 0 ]; then
  echo "hastd is running as PID $proc_pid"
else
  echo "hastd is not running"
  service hastd start
  echo "hastd started as PID $proc_pid"
fi

hastctl create $sharevol_name
hastctl role primary $sharevol_name

scp remotehastconf.sh root@$remoteip:/root/
ssh root@$remoteip sh remotehastconf.sh $(hostname) $localip $carpip $remotepool_name $remotevol_name $vol_size $vol_path $sharevol_name