# Replicating ZVol using HAST Framework.

Configuration Steps as follows:


- Navigate to the git cloned directory.

- Make the script executable using the command mentioned within quotes "# chmod +x hastconf.sh"

- Run the script by passing some values using the command mentioned within quotes "# ./hastconf.sh <remote_host> <remote_ip> <carp_ip> <remote_poolname> <vol_path> <vol_size> <hastvol_name>"

where, 

remote_host => hostname of the secondary node

remote_ip => IP address of the secondary node

carp_ip => Shared carp IP used for failover

remote_poolname => Pool name created already in secondary node

vol_path => zvol created in primary node, have to provide the full path of it.

vol_size => size of the created zvol in primary node, such as 15G/50G.

hastvol_name => User defined name for the hast volume.

**Automatic Failover Configuration**

Copy the content of this file "devd-conf.txt" into the file /etc/devd.conf  and then copy the script ‘carp-hast-switch.sh’ to the directory /usr/local/sbin/ and then restart the devd service.

"service devd restart"

That's it. ZFS volume has been successfully replicated using HAST. This can be verified by using command 'hastctl status'. By using this volume, you can create an iSCSI storage.
