#!/bin/sh

# Original script by Freddie Cash <fjwcash@gmail.com>
# Modified by Michael W. Lucas <mwlucas@BlackHelicopters.org>
# and Viktor Petersson <vpetersson@wireload.net>

# The names of the HAST resources, as listed in /etc/hast.conf
resources="hastvol"

# delay in mounting HAST resource after becoming master
# make your best guess
delay=3

# logging
log="local0.debug"
name="carp-hast"

# end of user configurable stuff

case "$1" in
        MASTER)
                logger -p $log -t $name "Switching to primary provider for ${resources}."
                sleep ${delay}

                ifconfig vmx0 vhid 1 advskew 1
                sysctl net.inet.carp.preempt=1

                # Wait for any "hastd secondary" processes to stop
                for disk in ${resources}; do
                        while $( pgrep -lf "hastd: ${disk} \(secondary\)" > /dev/null 2>&1 ); do
                                sleep 1
                        done

                        # Switch role for each disk
                        hastctl role primary ${disk}
                        if [ $? -ne 0 ]; then
                                logger -p $log -t $name "Unable to change role to primary for resource ${disk}."
                                exit 1
                        fi
                done

                # Wait for the /dev/hast/* devices to appear
                for disk in ${resources}; do
                        for I in $( jot 60 ); do
                                [ -c "/dev/hast/${disk}" ] && break
                                sleep 0.5
                        done

                        if [ ! -c "/dev/hast/${disk}" ]; then
                                logger -p $log -t $name "GEOM provider /dev/hast/${disk} did not appear."
                                exit 1
                        fi
                done

                logger -p $log -t $name "Role for HAST resources ${resources} switched to primary."


                logger -p $log -t $name "Mounting disks."
                for disk in ${resources}; do
                        #mkdir -p /hast/${disk}
                        #fsck -p -y -t ufs /dev/hast/${disk}
                        #mount /dev/hast/${disk} /hast/${disk}
                        service ctld onestart
                done

        ;;

        BACKUP)
                logger -p $log -t $name "Switching to secondary provider for ${resources}."

                ifconfig vmx0 vhid 1 advskew 7
                sysctl net.inet.carp.preempt=0

                if [ $(pgrep hastd) -ne 0 ]; then
                  logger -p $log -t $name "service is running as PID $(pgrep hastd)."
                else
                  logger -p $log -t $name "service is not running."
                  service hastd start
                  logger -p $log -t $name "service started as PID $(pgrep hastd)."
                fi

                # Switch roles for the HAST resources
                for disk in ${resources}; do
                        #if ! mount | grep -q "^/dev/hast/${disk} on "
                        #then
                        #else
                                #umount -f /hast/${disk}
                        #fi
                        service ctld onestop
                        sleep $delay
                        hastctl role secondary ${disk} 2>&1
                        if [ $? -ne 0 ]; then
                                logger -p $log -t $name "Unable to switch role to secondary for resource ${disk}."
                                exit 1
                        fi
                        logger -p $log -t $name "Role switched to secondary for resource ${disk}."
                done
        ;;
esac