#!/bin/sh

remotehost=$1
remoteip=$2
carpip=$3
pool_name=$4
vol_name=$5
vol_size=$6
remotevol_path=$7
sharevol_name=$8

interface=$(ifconfig | grep flags | cut -d : -f 1 | awk '{print $1; exit}')
localip=$(ifconfig $interface | grep inet | awk '{print $2; exit}')

zpool_name=$(zpool list -H | awk '{print$1; exit}')

if [ $pool_name == $zpool_name ] ; then
  zfs create -V $vol_size $pool_name"/"$vol_name
else
  echo "No such pool: $pool_name"
fi

vol_path="/dev/zvol/"$pool_name"/"$vol_name

file="/etc/hast.conf"

if [ -f $file ] ; then
  rm $file
  echo "listen tcp://0.0.0.0 " >> /etc/hast.conf
  echo "resource $sharevol_name {" >> /etc/hast.conf
  echo "on $remotehost {" >> /etc/hast.conf
  echo "local $remotevol_path" >> /etc/hast.conf
  echo "remote $localip }" >> /etc/hast.conf
  echo "on $(hostname) {" >> /etc/hast.conf
  echo "local $vol_path" >> /etc/hast.conf
  echo "remote $remoteip }" >> /etc/hast.conf
  echo "}" >> /etc/hast.conf
fi

proc_pid=$(pgrep -lf hastd | grep sbin | awk '{print $1}')

if [ $proc_pid -ne 0 ]; then
  echo "hastd is running as PID $proc_pid"
else
  echo "hastd is not running"
  service hastd start
  echo "hastd started as PID $proc_pid"
fi

hastctl create $sharevol_name
hastctl role secondary $sharevol_name